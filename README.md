# Elk iSSA Example

# Example code for elk iSSA examining effect
# of elk density on habitat selection via
# interaction between density at the start of
# the step and habitat at the end of a step.
# Using method from Muff et al. 2019 
# (10.1111/1365-2656.13087).
